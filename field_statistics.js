
/**
 * @file
 * AJAXify link to dynamically get field outputs.
 */

Drupal.behaviors.fieldStatistics = function(context) {
  // Store the default text of widget selects.
  $('.field-statistics-link', context).click(function(event) {
    $link = $(this).addClass('field-statistics-loading');
    $wrapper = $link.parent('.field-statistics-link-wrapper');
    if ($link.hasClass('field-statistics-content'))
      $field = $wrapper.parents('.field');
    else
      $field = $wrapper;

    $.getJSON($link.attr('href'), function(data) {
      if (data && data.status) {
        var $html = $('<div/>').html(data.html),
          id = 'field-' + String(Math.floor(Math.random() * 99999));

        $html.find(':first').attr('id', id);
        $field.replaceWith($html.html());
        $('#' + id, context).parent().attr('id', id + '-parent');

        Drupal.attachBehaviors('#' + id + '-parent');
      }
      else {
        alert(Drupal.t('Unable to get field value.'));
        $link.removeClass('field-statistics-loading');
      }
    });

    event.preventDefault();
  });
};
